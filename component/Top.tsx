import React, { FC } from 'react';

type Props = {
  fill: string;
  id?: string;
  className?: string;
  type: 'top' | 'bottom';
};

export const WaveSvg: FC<Props> = ({
  fill = '#FFC969',
  id,
  className,
  type,
}) => {
  if (type === 'top')
    return (
      <svg
        id={id}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
      >
        <path
          fill={fill}
          fillOpacity="1"
          d="M0,288L34.3,256C68.6,224,137,160,206,128C274.3,96,343,96,411,122.7C480,149,549,203,617,218.7C685.7,235,754,213,823,186.7C891.4,160,960,128,1029,138.7C1097.1,149,1166,203,1234,208C1302.9,213,1371,171,1406,149.3L1440,128L1440,0L1405.7,0C1371.4,0,1303,0,1234,0C1165.7,0,1097,0,1029,0C960,0,891,0,823,0C754.3,0,686,0,617,0C548.6,0,480,0,411,0C342.9,0,274,0,206,0C137.1,0,69,0,34,0L0,0Z"
        ></path>
      </svg>
    );

  return (
    <svg
      id={id}
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 1440 320"
    >
      <path
        fill={fill}
        fillOpacity="1"
        d="M0,224L60,240C120,256,240,288,360,256C480,224,600,128,720,106.7C840,85,960,139,1080,154.7C1200,171,1320,149,1380,138.7L1440,128L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"
      ></path>
    </svg>
  );
};
