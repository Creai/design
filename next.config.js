const withFonts = require('next-fonts');
const withReactSvg = require('next-react-svg');
const path = require('path');

module.exports = withFonts(
  withReactSvg({
    include: path.resolve(__dirname, 'component'),
    webpack(config, options) {
      return config;
    },
  })
);
