import Head from 'next/head';
import Programming from '../component/programming.svg';
import Code from '../component/code.svg';
import Solution from '../component/solution.svg';
import { WaveSvg } from '../component/top';
// https://undraw.co/search
// https://www.blobmaker.app/
// https://getwaves.io/

export default function Home() {
  return (
    <div id="root">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://fonts.googleapis.com/css?family=Exo+2:400,700&display=swap"
          rel="stylesheet"
        />
        >
      </Head>
      <WaveSvg type="top" id="top" fill="#FFC969" />

      <nav>
        <a href="/">Company</a>
        <div>
          <a href="/" className="team shadow-md">
            Team
          </a>
          <a href="/">Career</a>
          <a href="/">Contact us</a>
          <a href="/">About us</a>
        </div>
      </nav>

      <main style={{ height: '100%' }}>
        <div
          style={{ display: 'flex', justifyContent: 'flex-end', height: 600 }}
        >
          <div className="box first" style={{ padding: 64 }}>
            <div style={{ width: 400 }}>
              <h1 className="text-4xl font-bold">We do stuff!</h1>
              <h2 className="text-lg">Software - Firmware - Hardware</h2>

              <h2 className="text-lg">
                Your <b>ideas</b>, our <b>solutions</b>
              </h2>
              <h2>Lets work together!</h2>
            </div>

            <Code />
          </div>
        </div>

        <WaveSvg type="bottom" fill="#FFE8AF" />
        <div
          className="box second"
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            height: 600,
            paddingRight: 64,
            backgroundColor: '#FFE8AF',
          }}
        >
          <Solution />
          <div style={{ width: 600, paddingRight: 128 }}>
            <h1 className="text-4xl font-bold">Yours team</h1>
            <h2 className="text-lg">Consists of ...</h2>

            <h2 className="text-lg font-bold">Lets work together!</h2>
          </div>
        </div>
        <WaveSvg type="top" fill="#FFE8AF" />
      </main>

      <footer>
        <WaveSvg type="bottom" id="bottom" fill="#FFC969" />
        <div className="flex">
          <div className="flex-1 text-gray-700 text-centerpx-4 py-2 m-2">
            Short
          </div>
          <div className="flex-1 text-gray-700 text-center  px-4 py-2 m-2">
            Medium length
          </div>
          <div className="flex-1 text-gray-700 text-center  px-4 py-2 m-2">
            Stuff
          </div>
        </div>
        <div>Created with ❤ by Someone</div>
      </footer>
      <style jsx global>{`
        .team {
          background-color: #ffaf38;
          // border
          border-radius: 5px;
          padding: 6px 16px;
        }

        a {
          padding: 2px 16px;
          font-weight: bold;
        }

        .bg-white {
          background: white;
        }

        .box {
          flex: 1;
          margin-top: 128px;
          margin-right: 128px;
          position: relative;
        }

        .first > svg {
          top: 0;
          right: 0;
        }

        .second {
          margin: 0 !important;
          justify-content: flex-end;
        }
        .second > svg {
          top: 0;
          left: 0;
        }

        .box > svg {
          // z-index: 0;
          position: absolute;
          // width: 400px;
          // height: 300px;
          margin-top: 46px;
          margin-left: 120px;
        }

        #top {
          position: absolute;
          top: 0;
          z-index: 0;
        }

        // #bottom {
        //   position: absolute;
        //   bottom: 32px;
        //   z-index: 0;
        // }

        #root {
          position: relative;
          background-color: #fff5d7;
          display: flex;
          flex-direction: column;
          min-height: 100vh;
        }

        nav {
          z-index: 1;
          padding: 32px;

          display: flex;
          justify-content: space-between;
        }

        main {
          flex: 1;
          z-index: 1;
        }

        footer {
          // padding-top: 64px;
          z-index: 1;
        }

        footer > div {
          margin-top: -32px;
          padding: 64px;
          padding-top: 0;
          background-color: #ffc969;
        }

        * {
          font-family: 'Exo 2', sans-serif;
          color: rgb(46, 52, 59);
        }

        html,
        body {
          font-size: 21px;
          padding: 0;
          margin: 0;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
